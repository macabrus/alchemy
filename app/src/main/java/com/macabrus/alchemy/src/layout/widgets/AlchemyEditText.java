package com.macabrus.alchemy.src.layout.widgets;

import android.content.Context;
import android.util.AttributeSet;

import com.macabrus.alchemy.src.utils.Read;

/**
 * Created by bernard on 29.01.18..
 */

//custom extension of edittext widget
public class AlchemyEditText extends android.support.v7.widget.AppCompatEditText {

    public AlchemyEditText(Context context, AttributeSet attrs){
        super(context,attrs);
        setTypeface(new Read(context).LoadFonts());
        setTextSize(24);
    }
    public AlchemyEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeface(new Read(context).LoadFonts());
        setTextSize(24);
    }

    public AlchemyEditText(Context context) {
        super(context);
        setTypeface(new Read(context).LoadFonts());
        setTextSize(24);
    }
}
