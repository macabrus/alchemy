package com.macabrus.alchemy.src;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.text.DecimalFormat;

/**
 * Created by bernard on 04.09.17..
 */

//it just formats percentages so they are nice n smooth-looking in chart
public class PercentageFormater implements IValueFormatter {

    private DecimalFormat mFormat;

    public PercentageFormater() {
        mFormat = new DecimalFormat("###,###.00"); // use one decimal
    }

    @Override
    public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
        // write your logic here
        return mFormat.format(value) + "%"; // e.g. append a dollar-sign
    }
}
