package com.macabrus.alchemy.src.layout.widgets;

import android.content.Context;
import android.util.AttributeSet;

import com.macabrus.alchemy.R;
import com.macabrus.alchemy.src.utils.Read;

/**
 * Created by bernard on 31.12.17..
 */

//alchemy default textviews
public class AlchemyText extends android.support.v7.widget.AppCompatTextView {
    public AlchemyText(Context context, AttributeSet attrs){
        super(context,attrs);
        setTypeface(new Read(context).LoadFonts());
        setTextSize(24);
        setTextColor(getResources().getColor(R.color.textColor));
    }
    public AlchemyText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeface(new Read(context).LoadFonts());
        setTextSize(24);
        setTextColor(getResources().getColor(R.color.textColor));
    }

    public AlchemyText(Context context) {
        super(context);
        setTypeface(new Read(context).LoadFonts());
        setTextSize(24);
        setTextColor(getResources().getColor(R.color.textColor));
    }
}
