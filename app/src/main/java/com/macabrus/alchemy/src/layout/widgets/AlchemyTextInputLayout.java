package com.macabrus.alchemy.src.layout.widgets;

import android.content.Context;
import android.util.AttributeSet;

import com.macabrus.alchemy.src.utils.Read;

/**
 * Created by bernard on 29.01.18..
 */

//custom text input layouts
public class AlchemyTextInputLayout extends android.support.design.widget.TextInputLayout {
    public AlchemyTextInputLayout(Context context, AttributeSet attrs){
        super(context,attrs);
        setTypeface(new Read(context).LoadFonts());
    }
    public AlchemyTextInputLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeface(new Read(context).LoadFonts());
    }

    public AlchemyTextInputLayout(Context context) {
        super(context);
        setTypeface(new Read(context).LoadFonts());
    }
}
