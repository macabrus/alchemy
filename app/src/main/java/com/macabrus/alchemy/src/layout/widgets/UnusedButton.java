package com.macabrus.alchemy.src.layout.widgets;

import android.content.Context;
import android.util.AttributeSet;

import com.macabrus.alchemy.R;
import com.macabrus.alchemy.src.utils.Read;

/**
 * Created by bernard on 30.12.17..
 */

//custom button that represents feature that isn't yet available

public class UnusedButton extends android.support.v7.widget.AppCompatButton {
    public UnusedButton(Context context, AttributeSet attrs){
        super(context,attrs);
        setTypeface(new Read(context).LoadFonts());
        setTextSize(26);
        setTextColor(getResources().getColor(R.color.colorLightGrey));
    }
    public UnusedButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeface(new Read(context).LoadFonts());
        setTextSize(26);
        setTextColor(getResources().getColor(R.color.colorLightGrey));
    }

    public UnusedButton(Context context) {
        super(context);
        setTypeface(new Read(context).LoadFonts());
        setTextSize(26);
        setTextColor(getResources().getColor(R.color.colorLightGrey));
    }
}
