package com.macabrus.alchemy.src;

import java.util.ArrayList;

/**
 * Created by bernard on 02.12.17..
 */

public class Ratio {
    public static ArrayList<Integer> smallestIntRatio(ArrayList<Double> list){
        for(int x=0; x < list.size();x++){
            if (Math.abs(list.get(x))-Math.abs(list.get(x).intValue()) > 10e-5){
                System.out.println(x);
                Double holdxth = Math.abs(list.get(x))-Math.abs(list.get(x).intValue());
                for (int i = 0; i < list.size(); i++){
                    //System.out.print(list.get(i));
                    //System.out.print("/");
                    //System.out.println(holdxth);
                    list.set(i,list.get(i)/holdxth);
                }
                x = 0;
            }
        }
        ArrayList<Integer> returnlist = new ArrayList<>();
        for(int i = 0; i < list.size(); i++){
            returnlist.add(list.get(i).intValue());
        }
        return returnlist;
    }
}
