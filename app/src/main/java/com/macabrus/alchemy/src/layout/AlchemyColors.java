package com.macabrus.alchemy.src.layout;

import android.graphics.Color;

/**
 * Created by bernard on 11.03.18..
 */

public class AlchemyColors {

    //Graph color scheme
    public static int[] PASTEL_COLORS = new int[]{
            Color.rgb(64, 89, 128),
            Color.rgb(149, 165, 124),
            Color.rgb(217, 184, 162),
            Color.rgb(191, 134, 134),
            Color.rgb(179, 48, 80),
            Color.rgb(136,78,160),
            Color.rgb(41,128,185),
            Color.rgb(23,165,137),
            Color.rgb(34,153,84),
            Color.rgb(244,208,63),
            Color.rgb(151,154,154),
    };

    //alternate... not so good
    public int[] LIVELY_COLORS = new int[]{
            Color.rgb(192, 255, 140),
            Color.rgb(255, 247, 140),
            Color.rgb(255, 208, 140),
            Color.rgb(140, 234, 255),
            Color.rgb(255, 140, 157),
            Color.rgb(220,198,224),
            Color.rgb(174,168,211),
            Color.rgb(190,144,212),
            Color.rgb(197,239,247),
            Color.rgb(135,211,124),
            Color.rgb(238,238,238),
            Color.rgb(189,195,199)
    };
}
