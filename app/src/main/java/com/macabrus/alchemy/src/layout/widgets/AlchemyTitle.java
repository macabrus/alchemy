package com.macabrus.alchemy.src.layout.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.macabrus.alchemy.R;
import com.macabrus.alchemy.src.utils.Read;

/**
 * Created by bernard on 31.12.17..
 */

//alchemy title textviews
public class AlchemyTitle extends android.support.v7.widget.AppCompatTextView {
    public AlchemyTitle(Context context, AttributeSet attrs){
        super(context,attrs);
        setTypeface(new Read(context).LoadFonts(), Typeface.BOLD);
        setTextSize(30);
        setTextColor(getResources().getColor(R.color.textColor));
    }
    public AlchemyTitle(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeface(new Read(context).LoadFonts(), Typeface.BOLD);
        setTextSize(30);
        setTextColor(getResources().getColor(R.color.textColor));
    }

    public AlchemyTitle(Context context) {
        super(context);
        setTypeface(new Read(context).LoadFonts(), Typeface.BOLD);
        setTextSize(30);
        setTextColor(getResources().getColor(R.color.textColor));
    }
}
