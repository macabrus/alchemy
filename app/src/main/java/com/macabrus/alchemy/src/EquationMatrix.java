package com.macabrus.alchemy.src;

import java.util.ArrayList;


/**
 * Created by bernard on 04.12.17..
 */

public class EquationMatrix {
    private double [][] Matrix;
    public String rawEquation;
    private String Equation;
    private ArrayList<Compound> Compounds; //matrix y
    private ArrayList<String> AtomOrder; //matrix textIpt
    private ArrayList<Integer> Coefficients;
    private String[] FracturedEqn;
    private Integer numOfReactants=0;
    private ArrayList<String> CompoundNames;
    public boolean Valid;
/*
    private void printMatrix(){
        for(int i = 0; i < Matrix.length; i++){
            for(int j = 0; j < Matrix[0].length; j++){
                System.out.print(Matrix[i][j]);
                System.out.print(" ");
            }
            System.out.println("");
        }
    }
*/

    public EquationMatrix(String equation){
        Equation = equation.replace("=", "->").replace(" ","");
        rawEquation = equation;
        Compounds = new ArrayList<>();
        AtomOrder = new ArrayList<>();
        CompoundNames = new ArrayList<>();
        //fractures equation on molecules and calls Compound class to parse them
        //if it doesn't succeed, ergo there is error in input string and equation is invalid (obviously)
        if (!fillCompoundsList()) Valid = false;
        else Valid = true;
        try {

            //gets info about number of different atoms in all compounds and number of compouds
            fillAtomOrder();
            //makes corresponding matrix
            Matrix = new double[Compounds.size()][AtomOrder.size() + Compounds.size()];

            //fills matrix with
            fillMatrix();

            //augments corresponding square identity matrix to the right hand side
            augmentIdentityMatrix();
            printMatrix();
            //reduces matrix
            RREF.RREF(Matrix);
            printMatrix();
            //debugging purposes
            //System.out.println(getSolutionVector());

            //this takes soluton vector out of matrix
            //(downmost right row (corresponding to the number of compounds involved))
            //smallestIntRatio iterates over them and returns integer type solution vector
            //keeping their ratios. documentation explains why it is always possible.
            Coefficients = Ratio.smallestIntRatio(getSolutionVector());
            checkSolution();
            //System.out.println(Coefficients);
        }catch (Exception e){
            Valid = false;
            return;
        }
        //creates new string - solved equation
        solveEquation();
    }

    public String getEquation(){
        if(Valid&&!Equation.equals("")) return Equation;
        Valid = false;
        return "Invalid equation.";
    }

    public void solveEquation(){
        Equation = "";
        int i = 0;
        int j = 0;
        while(i < Coefficients.size()){
            if(Coefficients.get(i) <= 0){
                return;
            }
            if(FracturedEqn[j].equals("->")) j++;
            CompoundNames.add(FracturedEqn[i]);
            Integer coeff = Math.abs(Coefficients.get(i));
            FracturedEqn[j] = (coeff==1?"":coeff.toString()+" ")+
                              FracturedEqn[j];
            i++;
            j++;
        }
        for(i = 0; i < FracturedEqn.length; i++){
            if(i==numOfReactants){
                Equation+=FracturedEqn[i]+" ";
                Equation+="-> ";
            }
            else if(i==FracturedEqn.length-1){
                Equation+=FracturedEqn[i];
            }
            else{
                Equation+=FracturedEqn[i]+" + ";
            }
        }
        Equation = Equation.replace("+ -> ->","->");
    }
    private ArrayList<Double> getSolutionVector(){
        ArrayList<Double> SolutionVector = new ArrayList<>();
        for(int i = AtomOrder.size(); i < Matrix[0].length; i++){
            SolutionVector.add(Matrix[Compounds.size()-1][i]);
        }
        return SolutionVector;
    }
    private void augmentIdentityMatrix(){
        for(int i=0; i < Matrix.length; i++){
            Matrix[i][i+AtomOrder.size()]=1;
            /*
            1 0 0 0
            0 1 0 0
            0 0 1 0
            0 0 0 1 ...
            */
        }
    }
    private boolean fillCompoundsList(){
        boolean reactant = true;
        FracturedEqn = Equation.replace("->","+->+").replaceAll(" ","+").split("\\+");
        for(int i = 0; i < FracturedEqn.length; i++){
            if (FracturedEqn[i].equals("->")){
                reactant = false;
                numOfReactants=i;
                continue;
            }
            if(new Compound(FracturedEqn[i],reactant).Valid)
                Compounds.add(new Compound(FracturedEqn[i],reactant));
            else return false;
            System.out.println(new Compound(FracturedEqn[i],reactant).Compound);
        }
        return !reactant;
    }
    private void fillAtomOrder(){
        for(int i = 0; i < Compounds.size(); i++){
            for(int j = 0; j < Compounds.get(i).AtomTypes.size(); j++){
                if(!AtomOrder.contains(Compounds.get(i).AtomTypes.get(j))){
                    AtomOrder.add(Compounds.get(i).AtomTypes.get(j));
                }
            }
        }
    }
    private void fillMatrix(){
        for(int i = 0; i < Compounds.size(); i++){
            for(int j = 0; j < AtomOrder.size(); j++){
                if(Compounds.get(i).Compound.containsKey(AtomOrder.get(j))){
                    Matrix[i][j]=Compounds.get(i).Compound.get(AtomOrder.get(j));
                }
            }
        }
    }
    private void printMatrix(){
        System.out.println("MATRIX");
        for(int i = 0; i < Matrix.length; i++){
            for(int j = 0; j < Matrix[1].length; j++){
                System.out.print(Matrix[i][j]);
                System.out.print('\t');
            }
            System.out.print('\n');
        }
    }
    private void checkSolution(){
        for(int i = 0; i < AtomOrder.size(); i++){
            int sum = 0; //must stay 0 for all atoms in AtomOrder
            for(int j = 0; j < Compounds.size(); j++){
                if (Compounds.get(j).Compound.containsKey(AtomOrder.get(i)))
                    sum+=Compounds.get(j).Compound.get(AtomOrder.get(i))*Coefficients.get(j);
            }
            if (sum!=0){ Valid=!Valid;return;}
        }
    }
}
