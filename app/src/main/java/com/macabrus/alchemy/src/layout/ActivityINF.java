package com.macabrus.alchemy.src.layout;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.macabrus.alchemy.R;

/**
 * Created by bernard on 09.03.18..
 */

public class ActivityINF extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inf);
    }
}