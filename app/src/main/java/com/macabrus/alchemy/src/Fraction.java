package com.macabrus.alchemy.src;

public final class Fraction {

    private final int numerator;
    private final int denominator;

    /**
     * Constructor that allows you to specify whether or not to reduce the fraction.
     * @param numerator the numerator for the fraction. Must be >= 0.
     * @param denominator the denominator for the fraction. Must be > 0.
     * @param reduce true to reduce the fraction, false to leave it as is.
     * @throws IllegalArgumentException if either the numerator or denominator is invalid.
     */
    public Fraction(final int numerator, final int denominator, final boolean reduce) {
        if (numerator < 0) {
            throw new IllegalArgumentException("Numerator must be non-negative, was " + numerator);
        }

        if (denominator <= 0) {
            throw new IllegalArgumentException("Numerator must be positive, was " + numerator);
        }

        if (reduce) {
            final int gcd = this.greatestCommonDivisorOf(numerator, denominator);
            this.numerator = numerator / gcd;
            this.denominator = denominator / gcd;
        } else {
            this.numerator = numerator;
            this.denominator = denominator;
        }
    }

    /**
     * Constructor. This fraction will be automatically reduced.
     * @param numerator the numerator for the fraction. Must be >= 0.
     * @param denominator the denominator for the fraction. Must be > 0.
     * @throws IllegalArgumentException if either the numerator or denominator is invalid.
     */
    public Fraction(final int numerator, final int denominator) {
        this(numerator, denominator, true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return String.valueOf(this.numerator) + "/" + String.valueOf(this.denominator);
    }

    /**
     * Adds another fraction to this one. The resulting denominator will be the product of the two addends'
     * denominators.
     * @param addend the fraction to add to this one. May not be null.
     * @return a new Fraction instance that contains the result of the addition. Will never return null.
     */
    public Fraction plus(final Fraction addend) {
        return new Fraction(
                (this.numerator * addend.denominator) + (addend.numerator * this.denominator),
                this.denominator * addend.denominator);
    }

    /**
     * Subtracts another fraction from this one. The resulting denominator will be the product of the denominators
     * of the minuend (this fraction) and subtrahend (parameter fraction).
     * @param subtrahend the fraction to subtract from this one. May not be null.
     * @return a new Fraction instance that contains the result of the addition. Will never return null.
     */
    public Fraction minus(final Fraction subtrahend) {
        return new Fraction(
                (this.numerator * subtrahend.denominator) - (subtrahend.numerator * this.denominator),
                this.denominator * subtrahend.denominator);
    }

    /**
     * Multiplies this fraction by another fraction.
     * @param factor the fraction to multiply this one by. May not be null.
     * @return a new instance of a fraction containing the result of the multiplication. Will never return null.
     */
    public Fraction times(final Fraction factor) {
        return new Fraction(
                this.denominator * factor.denominator,
                this.numerator * factor.numerator);
    }

    /**
     * Divides this fraction by another fraction.
     * @param divisor the fraction to divide this one by. May not be null.
     * @return a new instance of a fraction containing the result of the division. Will never return null.
     */
    public Fraction dividedBy(final Fraction divisor) {
        final Fraction reciprocal = new Fraction(divisor.denominator, divisor.numerator);
        return this.times(reciprocal);
    }

    /**
     * Reduces this fraction to its simplest form by dividing the numerator and denominator by their greatest
     * common divisor.
     * @return a new instance of the fraction in reduced form. Will never return null.
     */
    public Fraction reduce() {
        final int gcd = this.greatestCommonDivisorOf(this.numerator, this.denominator);
        return new Fraction(this.numerator / gcd, this.denominator / gcd);
    }

    private int greatestCommonDivisorOf(final int firstNumber, final int secondNumber) {
        if (secondNumber == 0) {
            return firstNumber;
        }
        return this.greatestCommonDivisorOf(secondNumber, firstNumber % secondNumber);
    }
}