package com.macabrus.alchemy.src.layout;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.macabrus.alchemy.R;
import com.macabrus.alchemy.src.layout.widgets.AlchemyRecentEntry;
import com.macabrus.alchemy.src.layout.widgets.AlchemyRecentOnClickListener;
import com.macabrus.alchemy.src.utils.Read;

import java.util.ArrayList;

public class ActivityRCT extends AppCompatActivity {

    //I HAVE TO USE RECYCLERVIEW WITH ADAPTER WHICH WILL PUT ALL THE BUTTONS AS ITS ENTRIES.... GOTA FIGURE THIS OUT.
    private static ActivityRCT mContext;
    LinearLayout recent_buttons;
    ImageButton delete_button;
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_rct);
        delete_button = (ImageButton) findViewById(R.id.rct_button_clear);
        recent_buttons = (LinearLayout) findViewById(R.id.recent_entry_list);
        delete_button.setOnClickListener(button_delete_action);
        mContext = this;
        refreshList();
    }
    View.OnClickListener button_delete_action = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Read.clearRecent();
            refreshList();
        }
    };

    //@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void refreshList(){
        recent_buttons.removeAllViews();
        ArrayList<String> buttons_list = Read.readFromRecent();
        System.out.println(buttons_list);
        for (int i = buttons_list.size()-1; i >-1; i--){
            AlchemyRecentEntry recent_entry_button = new AlchemyRecentEntry(this);
            //THERE IS A BUG IN THIS "1". SOMEONE HAD A CRASH BUT NO FURTHER INFORMATION WAS RECEIVED. ROGER THAT.
            recent_entry_button.setText(buttons_list.get(i).split("#")[1]);
            AlchemyRecentOnClickListener presetMMC = new AlchemyRecentOnClickListener(buttons_list.get(i),this);
            recent_entry_button.setOnClickListener(presetMMC);
            //recent_buttons.setAdapter();
            recent_buttons.addView(recent_entry_button);
        }
    }

    public static ActivityRCT getContext(){
        return mContext;
    }
}
