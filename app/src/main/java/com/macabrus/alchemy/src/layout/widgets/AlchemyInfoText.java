package com.macabrus.alchemy.src.layout.widgets;

import android.content.Context;
import android.util.AttributeSet;

import com.macabrus.alchemy.R;
import com.macabrus.alchemy.src.utils.Read;

/**
 * Created by bernard on 15.02.18..
 */

//custom extension of edittext widget
public class AlchemyInfoText extends android.support.v7.widget.AppCompatTextView {
    public AlchemyInfoText(Context context, AttributeSet attrs){
        super(context,attrs);
        setTypeface(new Read(context).LoadFonts());
        setTextSize(15);
        setTextColor(getResources().getColor(R.color.textColor));
    }
    public AlchemyInfoText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeface(new Read(context).LoadFonts());
        setTextSize(15);
        setTextColor(getResources().getColor(R.color.textColor));
    }

    public AlchemyInfoText(Context context) {
        super(context);
        setTypeface(new Read(context).LoadFonts());
        setTextSize(15);
        setTextColor(getResources().getColor(R.color.textColor));
    }
}