package com.macabrus.alchemy.src.layout;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.macabrus.alchemy.R;

public class ActivityHelp extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
    }
}
