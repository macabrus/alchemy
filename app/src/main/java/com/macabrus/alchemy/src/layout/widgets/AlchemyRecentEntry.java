package com.macabrus.alchemy.src.layout.widgets;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

import com.macabrus.alchemy.src.utils.Read;
import com.macabrus.alchemy.R;

/**
 * Created by bernard on 24.02.18..
 */

//custom recent entry buttons
public class AlchemyRecentEntry extends AppCompatButton {
    //@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public AlchemyRecentEntry(Context context, AttributeSet attrs){
        super(context,attrs);
        setTypeface(new Read(context).LoadFonts());
        setTextSize(20);
        setTextColor(getResources().getColor(R.color.textColor));
        setBackgroundResource(R.drawable.recent_entry);
        setAllCaps(false);
        //setTextAlignment(TEXT_ALIGNMENT_TEXT_START);
    }
    //@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public AlchemyRecentEntry(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeface(new Read(context).LoadFonts());
        setTextSize(20);
        setTextColor(getResources().getColor(R.color.textColor));
        setBackgroundResource(R.drawable.recent_entry);
        setAllCaps(false);
        //setTextAlignment(TEXT_ALIGNMENT_TEXT_START);
    }

    //@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public AlchemyRecentEntry(Context context) {
        super(context);
        setTypeface(new Read(context).LoadFonts());
        setTextSize(20);
        setTextColor(getResources().getColor(R.color.textColor));
        setBackgroundResource(R.drawable.recent_entry);
        setAllCaps(false);
        //REQUIRES JELLY BEAN!!! WORKAROUND NEEDED.
        //setTextAlignment(TEXT_ALIGNMENT_TEXT_START);
    }
}
