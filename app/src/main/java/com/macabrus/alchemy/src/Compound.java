package com.macabrus.alchemy.src;

import android.util.Pair;

import com.macabrus.alchemy.src.utils.Read;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by bernard on 04.12.17..
 */

public class Compound {

    //Raw compound string
    public String RawCompound;

    /*
    By asigning false,
    all values in "Compound" will be negative
    (needed when calculating RREF of matrix)
    */
    private boolean Reactant;

    //Final parsed compound
    public Map<String,Integer> Compound;

    public ArrayList<String> AtomTypes;

    public boolean Valid;

    public Compound(String compound, boolean reactant){
        this.RawCompound = compound;
        this.Compound = new HashMap<>(10);
        this.AtomTypes = new ArrayList<>(10);
        this.Reactant = reactant;
        Parse();
    }
    public Compound(String compound){
        this.RawCompound = compound;
        this.Compound = new HashMap<>(10);
        this.AtomTypes = new ArrayList<>(10);
        Reactant = true;
        try{
            Parse();
        }catch (Exception e){
            //nah
        }
    }
    public void echo(){
        System.out.println(Compound);
    }
    public String getMass(){
        double mass = 0;
        Iterator<Map.Entry<String, Integer>>i=Compound.entrySet().iterator();
        while(i.hasNext()){
            Map.Entry<String,Integer> Atom = i.next();
            if(!Read.AtomicWeights.containsKey(Atom.getKey()))
                return "Invalid formula.";
            mass+=Read.AtomicWeights.get(Atom.getKey())*Atom.getValue();
        }
        return mass==0?"Invalid formula.":Double.toString(Math.round(mass*10000.0)/10000.0);
    }
    private boolean isUppercase(int i){
        return (i < RawCompound.length() &&
                RawCompound.charAt(i)>='A' &&
                RawCompound.charAt(i)<='Z');
    }
    private boolean isLowercase(int i){
        return (i < RawCompound.length() &&
                RawCompound.charAt(i)>='a' &&
                RawCompound.charAt(i)<='z');
    }
    private boolean isDigit(int i){
        return (i < RawCompound.length() &&
                RawCompound.charAt(i)>='0' &&
                RawCompound.charAt(i)<='9');
    }
    private boolean isOut(int i){
        return i>RawCompound.length();
    }
    private void Parse(){
        if(!checkValidity()) return;
        int brackets = 0;
        int i = 0;
        //K: Atom name, V: Atom subscript (atom name can occur multiple times)
        ArrayList<Pair<String,Integer>> IDs = new ArrayList<>();
        while (i < RawCompound.length()){
            if(isUppercase(i)){
                IDs.add(new Pair<>(getAtomAt(i), getAtomSubAt(i)));
                if(!AtomTypes.contains(getAtomAt(i))){
                    AtomTypes.add(getAtomAt(i));
                }
                i+=getAtomAt(i).length();
            }
            else if(RawCompound.charAt(i) == '('){
                brackets++;
                IDs.add(new Pair<>("!",getBracketSubAt(i)));
                i++;
            }
            else if(RawCompound.charAt(i) == ')'){
                brackets--;
                if(brackets < 0) return;
                IDs = getRidOfBracket(IDs);
                i++;
            }
            else if(isDigit(i)){
                i++;
            }
            /*
            else if(RawCompound.charAt(i) == '^'){
                i++;
                if(!AtomTypes.contains("Charge")){
                    AtomTypes.add("Charge");
                }
                String coef = "";
                while(isDigit(RawCompound.charAt(i))){
                    coef+=RawCompound.charAt(i++);
                }
                if(RawCompound.charAt(i++)=='-')coef='-'+coef;
                IDs.add(new Pair<>("Charge", Integer.parseInt(coef)));
            }
            else{
                System.out.println("Malformed Compound.");
                return;
            }
            */
        }
        Valid = checkValidity();
        //if(!Valid) return;
        IDtoCompound(IDs);
        //System.out.println(Compound);
        return;
    }
    private boolean checkValidity(){
        int opening = 0;
        int closing = 0;
        if (RawCompound.length()==0) return false;
        for(int i = 0; i < RawCompound.length(); i++){
            if(!(isDigit(i)||
                isLowercase(i)||
                isUppercase(i)||
                RawCompound.charAt(i) == '('||
                RawCompound.charAt(i) == ')'/*||
                RawCompound.charAt(i) == '^'*/)){
                    return false;
            }
            opening += RawCompound.charAt(i)=='('?1:0;
            closing += RawCompound.charAt(i)==')'?1:0;
        }
        if(opening-closing!=0){
            return false;
        }
        return true;
    }
    private boolean hasSub(int i){
        while(isLowercase(i)) i++;
        return isDigit(i);
    }
    private int getAtomAndSubLen(int i){
        int j = i+1;
        while(isDigit(j) || isLowercase(j)){
            j++;
        }
        return j-i;
    }
    private String getAtomAt(int i){
        String atom = ""+RawCompound.charAt(i);
        i++;
        while(isLowercase(i)){
            atom+=RawCompound.charAt(i);
            i++;
        }
        return atom;
    }
    private Integer getAtomSubAt(int i){
        i+=getAtomAt(i).length();
        String sub = "";
        while (isDigit(i)){
            sub+=RawCompound.charAt(i);
            i++;
        }
        return sub.equals("")?1:Integer.parseInt(sub);
    }
    private Integer getSubAt(int i){
        String sub = "";
        while (!isOut(i)&&isDigit(i)){
            sub+=RawCompound.charAt(i);
            i++;
        }
        return sub.equals("")?1:Integer.parseInt(sub);
    }
    private Integer getBracketSubAt(int i){
        int matchingbracket = 1; i++;
        while(!isOut(i)&&matchingbracket!=0){
            if(RawCompound.charAt(i) == '('){
                matchingbracket++;
            }
            if(RawCompound.charAt(i) == ')'){
                matchingbracket--;
            }
            i++;
        }
        return matchingbracket!=0?-1:getSubAt(i);
    }
    private ArrayList<Pair<String,Integer>> getRidOfBracket
    (ArrayList<Pair<String,Integer>> IDs)
    {
        int i = IDs.size()-1;
        while(!IDs.get(i).first.equals("!")) i--;
        int b_index = IDs.get(i).second;
        IDs.remove(i);
        for (int j = i; j < IDs.size(); j++){
            IDs.set(j,new Pair<>(IDs.get(j).first,
                    IDs.get(j).second*b_index));
        }
        return IDs;
    }
    private void IDtoCompound(ArrayList<Pair<String,Integer>> IDs){
        Iterator<Pair<String,Integer>> i = IDs.iterator();
        int sign = Reactant?1:-1;
        while (i.hasNext()){
            Pair<String,Integer> Atom = i.next();
            if (Compound.containsKey(Atom.first)){
                Compound.put(Atom.first,
                             Compound.get(Atom.first)+
                             Atom.second*sign);
            }
            else{
                Compound.put(Atom.first,Atom.second*sign);
            }
        }
    }
}
