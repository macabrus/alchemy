package com.macabrus.alchemy.src.layout;

import android.content.Context;
import android.support.v4.content.ContextCompat;

import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;

import com.macabrus.alchemy.src.Compound;
import com.macabrus.alchemy.src.EquationMatrix;
import com.macabrus.alchemy.src.PercentageFormater;
import com.macabrus.alchemy.src.utils.Read;

import java.util.ArrayList;

/**
 * Created by bernard on 06.12.17..
 */

//Working with Compound class results (AtomicWeights hashmap) and putting it in bar data

public class Chart {

    private int pinky;
    BarData barData;
    //PieData pieData;

    //TO DO: graphs for CEB
    public Chart(EquationMatrix matrix, Read elementlist){
    }

    public Chart(Compound compound, Context c){
        if (!compound.Valid){
            barData = new BarData();
            return;
        }
        //pinky = ContextCompat.getColor(c, AlchemyColors);
        float mass = Float.parseFloat(compound.getMass());
        ArrayList<IBarDataSet> datasets = new ArrayList<>();
        for(int i = 0; i < compound.Compound.size(); i++){
            float current_share = compound.Compound.get(compound.AtomTypes.get(i))*
                                    Read.AtomicWeights.get(compound.AtomTypes.get(i));
            float res = current_share/mass*100;
            System.out.println(current_share);
            System.out.println(mass);
            System.out.println("------");
            //shares.add(new BarEntry(i,res));
            ArrayList<BarEntry> share = new ArrayList<>();
            share.add(new BarEntry(i,res));
            BarDataSet element_entry = new BarDataSet(share,compound.AtomTypes.get(i));
            element_entry.setColor(AlchemyColors.PASTEL_COLORS[i%AlchemyColors.PASTEL_COLORS.length]);
            element_entry.setValueTextColor(pinky);
            element_entry.setValueTextSize(15);
            element_entry.setValueFormatter(new PercentageFormater());
            element_entry.setValueTypeface(new Read(c).LoadFonts());
            datasets.add(element_entry);
        }
        barData = new BarData(datasets);
    }
}
