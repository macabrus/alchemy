package com.macabrus.alchemy.src.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by bernard on 31.08.17..
 */

//Reading from asset files

public class Read{

    private static Context mContext;
    public static Map<String, Float> AtomicWeights;

    public Read(Context c) {
        this.mContext = c;
    }

    public static void Plaintext() {
        AtomicWeights = new HashMap<>();

        AssetManager am = mContext.getAssets();

        try {
            InputStream is = am.open("elements.txt");
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String line;

            while (null != (line = reader.readLine())){
                String [] LN = line.split("\t");
                AtomicWeights.put(LN[0],Float.parseFloat(LN[1]));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Typeface LoadFonts(){
        return Typeface.createFromAsset(mContext.getAssets(),"fonts/quicksand.ttf");
    }

    public static ArrayList<String> readFromRecent(){

        ArrayList<String> content = new ArrayList<>();

        try{
            File path = mContext.getFilesDir();
            File file = new File(path, "recent.csv");
            FileInputStream in = new FileInputStream(file);
            byte[] bytes = new byte[(int) file.length()];
            in.read(bytes);
            in.close();
            content.addAll(Arrays.asList(new String(bytes).split("\n")));
        }catch (IOException e){
            e.printStackTrace();
        }
        return content;
    }

    public static void writeToRecent(String activity, String to_write){
        try {
            File path = mContext.getFilesDir();
            File file = new File(path, "recent.csv");
            if(!file.exists()) file.createNewFile();
            FileWriter stream = new FileWriter(file,true);
            stream.write(activity+"#"+to_write+"\n");
            stream.close();
        }catch (Exception e){
            //...
        }
    }
    public static void clearRecent(){
        File dir = mContext.getFilesDir();
        File file = new File(dir, "recent.csv");
        file.delete();
    }
}
