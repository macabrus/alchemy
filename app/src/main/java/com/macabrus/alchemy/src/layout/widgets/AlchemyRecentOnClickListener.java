package com.macabrus.alchemy.src.layout.widgets;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.macabrus.alchemy.src.layout.ActivityCEB;
import com.macabrus.alchemy.src.layout.ActivityMMC;

/**
 * Created by bernard on 08.03.18..
 */

//onclick listener for recent entries
public class AlchemyRecentOnClickListener implements View.OnClickListener {

    private String formula;
    private Context mContext;

    public AlchemyRecentOnClickListener(String s, Context c){
        formula = s; mContext = c;
    }

    @Override
    public void onClick(View v) {
        if(formula.split("#")[0].equals("MMC")){
            Intent i = new Intent(mContext,ActivityMMC.class);
            Bundle b = new Bundle();
            b.putString(formula.split("#")[0],formula.split("#")[1]);
            i.putExtras(b);
            mContext.startActivity(i);
        }
        else{
            Intent i = new Intent(mContext,ActivityCEB.class);
            Bundle b = new Bundle();
            b.putString(formula.split("#")[0],formula.split("#")[1]);
            i.putExtras(b);
            mContext.startActivity(i);
        }
    }
};
