package com.macabrus.alchemy.src.layout;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;

import com.macabrus.alchemy.R;
import com.macabrus.alchemy.src.Compound;
import com.macabrus.alchemy.src.layout.widgets.FlatButton;
import com.macabrus.alchemy.src.utils.Read;

//molecular mass calculator activity class

public class ActivityMMC extends AppCompatActivity {

    EditText txtIpt;        //molecule formula input field
    TextView result_info;   //text above resulting mass
    TextView result;        //mass value text
    BarChart shares_chart;  //chart for masses
    int alchemyTextColor;   //light pink color

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mmc);
        FlatButton BtnGetMass = findViewById(R.id.button_getmass);
        alchemyTextColor = ContextCompat.getColor(getApplicationContext(),R.color.textColor);
        txtIpt = findViewById(R.id.edit_text_mmc);
        checkIntentAttributes();
        result_info = findViewById(R.id.info_mmc);
        result = findViewById(R.id.result_mmc);
        //Elementlist = new Read(getApplicationContext());
        shares_chart = findViewById(R.id.mass_chart);
        shares_chart.setNoDataTextColor(alchemyTextColor);
        shares_chart.setNoDataText("Chart area.");
        shares_chart.setNoDataTextTypeface(Read.LoadFonts());
        BtnGetMass.setOnClickListener(button_getmass_action);
    }

    View.OnClickListener button_getmass_action = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Compound cpd = new Compound(txtIpt.getText().toString());
            if(cpd.getMass()!="Invalid formula."){
                result_info.setText("Relative molecular mass: ");
                result.setText(cpd.getMass());
                makeChart(cpd);
                hideKeyboard();
                Read.writeToRecent("MMC",cpd.RawCompound);
            }
            else{
                result_info.setText(cpd.getMass());
                result.setText("");
            }
            //update chart
        }
    };

    private void hideKeyboard(){
        LinearLayout mainLayout = findViewById(R.id.layoutMMC);
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(mainLayout.getWindowToken(), 0);
        }
        catch (Exception e){
            //just continue with program cuz its obviously already hidden...
        }
    }
    private void checkIntentAttributes(){
        Bundle b = getIntent().getExtras();
        //System.out.println(b.getString("MMC"));
        try {
            txtIpt.setText(b.getString("MMC"));
            hideKeyboard();
        }catch (NullPointerException e){
            //...
        }
    }
    private void makeChart(Compound cpd){
        Chart DATA = new Chart(cpd, getApplicationContext());
        //BarChart shares_chart = (BarChart) findViewById(R.id.mass_chart);
        //int alchemyTextColor = ContextCompat.getColor(getApplicationContext(),R.color.textColor);
        shares_chart.setEnabled(true);
        YAxis y_right = shares_chart.getAxisRight();
        Legend L = shares_chart.getLegend();
        XAxis x = shares_chart.getXAxis();
        YAxis y_left = shares_chart.getAxisLeft();

        y_left.setStartAtZero(true);
        y_left.setGridColor(alchemyTextColor);
        y_left.setTypeface(Read.LoadFonts());
        y_left.setDrawAxisLine(false);
        y_left.setTextSize(15);
        y_left.setTextColor(alchemyTextColor);
        y_left.setLabelCount(11);
        y_left.setGranularityEnabled(true);
        y_left.setGranularity(10);

        y_right.setDrawAxisLine(false);
        y_right.setDrawGridLines(false);
        y_right.setDrawLabels(false);

        x.setDrawLabels(false);
        x.setDrawGridLines(false);

        shares_chart.animateY(1000);
        shares_chart.getDescription().setText("");
        shares_chart.getAxisLeft().setAxisMaximum(100);
        shares_chart.setHighlightPerTapEnabled(false);
        shares_chart.setHighlightPerDragEnabled(false);
        shares_chart.setScaleYEnabled(false);
        L.setTextColor(alchemyTextColor);
        L.setTypeface(Read.LoadFonts());
        L.setTextSize(15);
        //L.setPosition(Legend.LegendPosition.RIGHT_OF_CHART_CENTER);
        shares_chart.setData(DATA.barData);
        shares_chart.invalidate();
    }
}
