package com.macabrus.alchemy.src;

/**
 * Created by bernard on 30.09.17..
 */

//IN CASE #1 MATRIX IS m x n WHERE m = n-1 THEN REDUCE IT AND AUGMENT ROW OF UNITY MATRIX (0,0,0,0,...,0,1)
    //AND DIVIDE ALL NUMBERS IN LAST COLUMN WITH SMALLEST OF THEM
// IN CARE #2 MATRIX IS n textIpt n THEN
// JUST REDUCE IT AND DIVIDE ALL BY SMALLEST OF DIMENSION OF LAST VECTOR
// (textIpt,y,z,d...)^T / min((textIpt,y,z,d...))^T

class RREF {
    public static void RREF(double[][] M) {
        int rowCount = M.length;
        if (rowCount == 0)
            return;

        int columnCount = M[0].length;

        int lead = 0;
        for (int r = 0; r < rowCount; r++) {
            if (lead >= columnCount)
                break;
            {
                int i = r;
                while (M[i][lead] == 0) {
                    i++;
                    if (i == rowCount) {
                        i = r;
                        lead++;
                        if (lead == columnCount)
                            return;
                    }
                }
                double[] temp = M[r];
                M[r] = M[i];
                M[i] = temp;
            }

            {
                double lv = M[r][lead];
                for (int j = 0; j < columnCount; j++)
                    M[r][j] /= lv;
            }

            for (int i = 0; i < rowCount; i++) {
                if (i != r) {
                    double lv = M[i][lead];
                    for (int j = 0; j < columnCount; j++)
                        M[i][j] -= lv * M[r][j];
                }
            }
            lead++;
        }
    }
}