package com.macabrus.alchemy.src.layout;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.macabrus.alchemy.R;
import com.macabrus.alchemy.src.EquationMatrix;
import com.macabrus.alchemy.src.utils.Read;

//activity for accessing EquationMatrix module
public class ActivityCEB extends AppCompatActivity {

    EditText textIpt;
    TextView BAL_EQ;
    TextView F_EQUATION;
    Button GETEQUATION;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ceb);
        GETEQUATION = (Button) findViewById(R.id.button_balance);

        textIpt = (EditText) findViewById(R.id.edit_text_ceb);
        BAL_EQ = (TextView) findViewById(R.id.info_ceb);
        F_EQUATION = (TextView) findViewById(R.id.result_ceb);

        //for some reason keyboard is not raised by default on this activity
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        try{
            Bundle b = getIntent().getExtras();
            if(b.containsKey("CEB")) textIpt.setText(b.getString("CEB"));
        }catch (Exception e){//...
        }

        GETEQUATION.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EquationMatrix eqMatrix = new EquationMatrix(textIpt.getText().toString());
                String TOTAL = eqMatrix.getEquation();
                BAL_EQ.setText("Balanced Equation:");
                F_EQUATION.setText(TOTAL);
                hideKeyboard();
                if (eqMatrix.Valid) Read.writeToRecent("CEB",eqMatrix.rawEquation);
            }
        });
    }
    private void hideKeyboard(){
        LinearLayout mainLayout = (LinearLayout)findViewById(R.id.text_input_ceb);
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(mainLayout.getWindowToken(), 0);
        }
        catch (Exception e){
            //just continue with program...
        }
    }
}
