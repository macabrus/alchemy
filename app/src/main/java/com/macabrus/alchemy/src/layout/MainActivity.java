package com.macabrus.alchemy.src.layout;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.macabrus.alchemy.R;
import com.macabrus.alchemy.src.layout.widgets.FlatButton;
import com.macabrus.alchemy.src.layout.widgets.UnusedButton;

public class MainActivity extends AppCompatActivity {

    //bunch of buttons (trust me there is no easier way)
    FlatButton ButtonMMC;
    Button ButtonCEB;
    Button ButtonPSE;
    Button ButtonRCT;
    FlatButton ButtonHLP;
    FlatButton ButtonINF;
    ImageButton ImgButtonMMC;
    ImageButton ImgButtonCEB;
    ImageButton ImgButtonPSE;
    ImageButton ImgButtonRCT;
    ImageButton ImgButtonHLP;
    ImageButton ImgButtonINF;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Read.Plaintext();
        //Read Quicksand = new Read(this);
        //FontsOverride.setDefaultFont(this, "SERIF", "fonts/quicksand.ttf");

        //pls no hate
        ButtonMMC = (FlatButton) findViewById(R.id.button_mmc);
        ButtonPSE = (UnusedButton) findViewById(R.id.button_pse);
        ButtonCEB = (FlatButton) findViewById(R.id.button_ceb);
        ButtonRCT = (FlatButton) findViewById(R.id.button_rct);
        ButtonHLP = (FlatButton) findViewById(R.id.button_hlp);
        ButtonINF = (FlatButton) findViewById(R.id.button_inf);

        ImgButtonMMC = (ImageButton) findViewById(R.id.img_button_mmc);
        ImgButtonCEB = (ImageButton) findViewById(R.id.img_button_ceb);
        ImgButtonPSE = (ImageButton) findViewById(R.id.img_button_pse);
        ImgButtonRCT = (ImageButton) findViewById(R.id.img_button_rct);
        ImgButtonHLP = (ImageButton) findViewById(R.id.img_button_hlp);
        ImgButtonINF = (ImageButton) findViewById(R.id.img_button_inf);

/*
        ButtonMMC.setTypeface(Quicksand.LoadFonts());
        ButtonCEB.setTypeface(Quicksand.LoadFonts());
        ButtonPSE.setTypeface(Quicksand.LoadFonts());
        ButtonRCT.setTypeface(Quicksand.LoadFonts());
*/
        //ugh...
        ButtonMMC.setOnClickListener(MMC);
        ButtonCEB.setOnClickListener(CEB);
        ButtonHLP.setOnClickListener(HLP);
        ButtonINF.setOnClickListener(INF);
        ImgButtonMMC.setOnClickListener(MMC);
        ImgButtonCEB.setOnClickListener(CEB);
        ImgButtonHLP.setOnClickListener(HLP);
        ImgButtonINF.setOnClickListener(INF);
        //ButtonPSE.setOnClickListener(PSE);
        ButtonRCT.setOnClickListener(RCT);
    }
    View.OnClickListener HLP = new View.OnClickListener(){
        @Override
        public void onClick(View view){
            loadHLP(view);
        }
    };
    View.OnClickListener MMC = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            loadMMC(view);
        }
    };
    View.OnClickListener CEB = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            loadCEB(view);
        }
    };
    View.OnClickListener RCT = new View.OnClickListener(){
        @Override
        public void onClick(View view){
            loadRCT(view);
        }
    };
    View.OnClickListener INF = new View.OnClickListener(){
        @Override
        public void onClick(View view){
            loadINF(view);
        }
    };
    /*
    View.OnClickListener PSE = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            loadPSE(view);
        }
    };
    View.OnClickListener RCT = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            loadRCT(view);
        }
    };
    */

    public void loadMMC(View v) {
        Intent intent = new Intent(this, ActivityMMC.class);
        startActivity(intent);
    }

    public void loadCEB (View v) {
        Intent intent = new Intent(this, ActivityCEB.class);
        startActivity(intent);
    }

    public void loadHLP(View v){
        Intent intent = new Intent(this, ActivityHelp.class);
        startActivity(intent);
    }
    public void loadRCT(View v){
        Intent intent = new Intent(this, ActivityRCT.class);
        startActivity(intent);
    }
    public void loadINF(View v){
        Intent intent = new Intent(this, ActivityINF.class);
        startActivity(intent);
    }
    /*
    public void loadPSE (View v){
        Intent intent = new Intent(this,ActivityPSE.class);
    }
    public void loadRCT(View v){
        Intent intent = new Intent(this,ActivityRCT.class);
    }
    */
    /*@Override
    protected void*/
}
